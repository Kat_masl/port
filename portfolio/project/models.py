from django.db import models
from portfolio.models import Student

class Projects(models.Model):
    nikname = models.ManyToManyField(to=Student, related_name='nikname', null=True, blank=True)
    project = models.CharField('Проект', max_length=200, null=True, blank=True)
    description = models.TextField('description')
    file_project = models.URLField('Файл_проекта')
    file_document = models.URLField('Документ')
    event = models.CharField('Мероприятие', max_length=80)
    result = models.PositiveSmallIntegerField('Результат', null=True, blank=True)

    def __str__(self):#Вывод объекта
        return self.event
