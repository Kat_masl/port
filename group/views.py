from django.shortcuts import render, get_object_or_404
from .models import Groupin

def groups(request):
    groups = Groupin.objects.all()
    return render(request, 'portfolio/group.html', {'groups':groups})

def group(request, id_group: int):
    group = get_object_or_404(Groupin,id=id_group)
    return render(request, 'portfolio/group1.html', {'group':group})
